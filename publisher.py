import tableauserverclient as TSC
import os

from dotenv import load_dotenv

load_dotenv()

tableau_auth = TSC.PersonalAccessTokenAuth(os.getenv("TOKEN_NAME"), os.getenv("TOKEN_VALUE"), os.getenv("SITENAME"))
server = TSC.Server(os.getenv("SERVER_URL"), use_server_version=True)
server.auth.sign_in(tableau_auth)

with server.auth.sign_in(tableau_auth):
    all_project_items, pagination_item = server.projects.get()
    publish_project = list(filter(lambda project: project.name == os.getenv("DASHBOARD_PROJECT"), all_project_items))[0]

    wb_item = TSC.WorkbookItem(name=os.getenv("DASHBOARD_SERVER_NAME"), project_id=publish_project.id)
    server.workbooks.publish(wb_item, os.getenv("DASHBOARD_FILENAME"), 'Overwrite')
    print('Dashboard uploaded succesfully')
